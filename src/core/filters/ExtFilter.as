package core.filters {
import core.effects.ExtMeshEffect;

import starling.display.DisplayObject;
import starling.events.Event;
import starling.filters.FragmentFilter;
import starling.rendering.FilterEffect;

public class ExtFilter extends FragmentFilter {

    public function ExtFilter() {
        alwaysDrawToBackBuffer = true;
    }

    override protected function createEffect():FilterEffect {
        return new ExtMeshEffect();
    }

    override protected function onTargetAssigned(target:DisplayObject):void {
        super.onTargetAssigned(target);
        addEventListener(Event.ENTER_FRAME, enterFrameHandler);
    }

    private function enterFrameHandler(event:Event):void {
        setRequiresRedraw();
    }
}
}
