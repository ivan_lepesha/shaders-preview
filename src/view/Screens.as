package view {
import core.ScreenItemData;

import view.screens.MainScreen;

public class Screens {

    public static const MAIN_SCREEN:String = "main_screen";
    public static const LOADING_SCREEN:String = "loading_screen";

    public static var _screensData:Vector.<ScreenItemData> = createScreens();

    private static function createScreens():Vector.<ScreenItemData> {
        var v:Vector.<ScreenItemData> = new <ScreenItemData>[];

        v.push(new ScreenItemData(MainScreen, MAIN_SCREEN));
        v.push(new ScreenItemData(MainScreen, LOADING_SCREEN));

        return v;
    }

    public static function getAll():Vector.<ScreenItemData> {
        return _screensData;
    }
}
}
