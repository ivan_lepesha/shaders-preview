package {
import feathers.layout.HorizontalAlign;
import feathers.layout.VerticalAlign;

import flash.display.Sprite;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.events.Event;

import starling.core.Starling;
import starling.events.Event;

import view.App;

[SWF(frameRate="60", backgroundColor="#777777", width="1000", height="700")]

public class Main extends Sprite {
    public function Main() {
        super();

        if (stage) {
            init();
        }
        else {
            addEventListener(flash.events.Event.ADDED_TO_STAGE, function (event:flash.events.Event):void {
                removeEventListener(flash.events.Event.ADDED_TO_STAGE, arguments.callee);
                init();
            });
        }
    }

    private function init():void {
        stage.scaleMode = StageScaleMode.NO_SCALE;
        stage.align = StageAlign.TOP_LEFT;
        loaderInfo.addEventListener(flash.events.Event.COMPLETE, onLoadComplete);
    }

    private function onLoadComplete(event:flash.events.Event):void {
        loaderInfo.removeEventListener(flash.events.Event.COMPLETE, onLoadComplete);
        startStarling();
    }

    private function startStarling():void {
        var s:Starling = new Starling(App, stage);
        s.addEventListener(starling.events.Event.ROOT_CREATED, onRootCreated);
        s.showStats = true;
        s.showStatsAt(HorizontalAlign.RIGHT, VerticalAlign.BOTTOM);
        s.start();
    }

    private function onRootCreated(event:starling.events.Event):void {
        var root:App = event.data as App;
        if (root) root.run();
    }
}
}
