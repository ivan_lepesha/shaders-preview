package core.effects {
import flash.display3D.Context3D;
import flash.display3D.Context3DProgramType;
import flash.utils.getTimer;

import starling.rendering.MeshEffect;
import starling.rendering.Program;

public class WaterMeshEffect extends MeshEffect {

    public function WaterMeshEffect() {

    }

    override protected function beforeDraw(context:Context3D):void {
        var ti:Number  = getTimer() / 1000;

        var fc0:Vector.<Number> = new <Number>[ti, 15.0, 0.02, 1]; //[time, size, c2, 1]

        context.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 0, fc0);

        super.beforeDraw(context);
    }

    override protected function createProgram():Program {
        var vertexShader:String, fragmentShader:String;

        vertexShader =
                "m44 op, va0, vc0 \n" +
                "mov v0, va1      \n" +
                "mul v1, va2, vc4 \n";

        fragmentShader =
                "mov ft0.xy, v0.xy \n" +
                "mul ft0.xy, ft0.xy, fc0.yy \n" +
                "add ft0.xy, ft0.xy, fc0.xx \n" +

                "add ft2.x, ft0.x, ft0.y  \n" +
                "cos ft2.x, ft2.x  \n" +

                "sub ft2.y, ft0.x, ft0.y  \n" + //x - y
                "sin ft2.y, ft2.y \n" + //sin(x - y)

                "cos ft2.z, ft0.y  \n" + //cos(y)
                "sin ft2.w, ft0.y  \n" + //sin(y)

                "mul ft3.x, ft2.x, fc0.z  \n" + // cos(x + y) * 0.01
                "mul ft3.y, ft2.y, fc0.z  \n" + // sin(x - y) * 0.01

                "mul ft5.y, ft3.x, ft2.z  \n" + //cos(x + y) * 0.01 * cos(y)
                "mul ft5.x, ft3.y, ft2.w  \n" + //sin(x - y) * 0.01 * sin(y)

                "mov ft6.xy, v0.xy \n" + //mov uv
                "add ft6.x, ft6.x, ft5.y \n" + //uv.x + (sin(x - y) * 0.01 * sin(y))
                "add ft6.y, ft6.y, ft5.x \n" + //uv.y + (cos(x + y) * 0.01 * cos(y))

                "tex ft4, ft6.xy, fs0 <linear 2d repeat> \n" +
                "mov oc ft4 \n";

        return Program.fromSource(vertexShader, fragmentShader, 1);
    }
}
}
