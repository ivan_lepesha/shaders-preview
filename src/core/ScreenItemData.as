package core {
public class ScreenItemData {

    private var _screen:Object;
    private var _id:String;

    public function ScreenItemData(screen:Object, id:String) {
        _screen = screen;
        _id = id;
    }

    public function get screen():Object {
        return _screen;
    }

    public function get id():String {
        return _id;
    }
}
}
