package core.filters {
import core.effects.WhiteToColorEffect;

import starling.display.DisplayObject;
import starling.events.Event;
import starling.filters.FragmentFilter;
import starling.rendering.FilterEffect;

public class WhiteToColorFilter extends FragmentFilter {

    public function WhiteToColorFilter() {
        alwaysDrawToBackBuffer = true;
    }

    override protected function createEffect():FilterEffect {
        return new WhiteToColorEffect();
    }

    override protected function onTargetAssigned(target:DisplayObject):void {
        super.onTargetAssigned(target);
        addEventListener(Event.ENTER_FRAME, enterFrameHandler);
    }

    private function enterFrameHandler(event: Event):void
    {
        setRequiresRedraw();
    }
}
}
