package assets {
import flash.display.Bitmap;

import starling.textures.Texture;

public class Images {

    [Embed(source="wood.png")]
    public static const wood:Class;
    [Embed(source="bamboo.jpg")]
    public static const bamboo:Class;
    [Embed(source="zebra.png")]
    public static const zebra:Class;
    [Embed(source="water.jpg")]
    public static const water:Class;


    private static var instance:Images;

    public function Images() {
        if(instance) {
            throw new Error("Images is singleton, user get instance instead");
        }
        else
        {
            init();
        }
    }

    public static function getInstance():Images {
        if(instance == null) {
            instance = new Images();
        }

        return instance;
    }

    private function init():void {
    }

    public function getTexture(clazz:Class):Texture {
        return Texture.fromBitmap(new clazz() as Bitmap)
    }
}
}
