package core.effects {
import flash.display3D.Context3D;
import flash.display3D.Context3DProgramType;
import flash.utils.getTimer;

import starling.rendering.MeshEffect;
import starling.rendering.Program;

public class WhiteToColorEffect extends MeshEffect {

    public function WhiteToColorEffect() {

    }

    override protected function beforeDraw(context:Context3D):void {
        var ti:Number  = getTimer() / 1000;

//        var fc0:Vector.<Number> = new <Number>[1, 15.0, 0.02, 1]; //[time, size, c2, 1]
        var fc1:Vector.<Number> = new <Number>[ti, 0.5, 0.2, 0.6]; //[ 1, 0, 0, 1]

//        context.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 0, fc0);
        context.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 1, fc1);

        super.beforeDraw(context);
    }

    override protected function createProgram():Program {
        var vertexShader:String, fragmentShader:String;

        vertexShader =
                "m44 op, va0, vc0 \n" +
                "mov v0, va1      \n" +
                "mul v1, va2, vc4 \n";

        fragmentShader =
                "tex ft0, v0, fs0 <linear 2d>\n" +
                "mov ft1, fc1.xxxx \n" +
                "mul ft1, ft1, fc1.yyyy \n" +
                "mul ft1.y, ft1.y, fc1.z \n" +
                "mul ft1.z, ft1.z, fc1.w \n" +
                "mul ft1.z, ft1.z, v0.x \n" +
                "sin ft1, ft1 \n" +
                "abs ft1, ft1 \n" +

                "mul oc, ft0, ft1 \n";

        return Program.fromSource(vertexShader, fragmentShader, 1);
    }
}
}
