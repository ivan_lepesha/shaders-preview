package view {
import controller.AppController;

import core.ScreenItemData;

import feathers.controls.ScreenNavigator;
import feathers.controls.ScreenNavigatorItem;
import feathers.motion.Fade;

import model.Model;

import starling.animation.Transitions;
import starling.events.Event;

public class AppScreenNavigator extends ScreenNavigator {

    private var _model:Model;
    private var fade: Function;

    public function AppScreenNavigator() {
        fade = Fade.createFadeInTransition( 0.75, Transitions.EASE_IN_OUT );
    }

    public function init(model:Model, controller:AppController):void {
        _model = model;
        _model.addEventListener("currentScreenChanged", screenChangedHandler);

        var data:Vector.<ScreenItemData> = Screens.getAll();
        for each(var item:ScreenItemData in data) {
            var screenItem:ScreenNavigatorItem = new ScreenNavigatorItem(item.screen, null, {model:model, controller:controller});
            addScreen(item.id, screenItem);
        }
    }

    private function screenChangedHandler(event:Event):void {
        showScreen(_model.currentScreen, fade);
    }
}
}
