package core.filters {
import core.effects.WaterMeshEffect;

import starling.display.DisplayObject;
import starling.events.Event;
import starling.filters.FragmentFilter;
import starling.rendering.FilterEffect;

public class WaterFilter extends FragmentFilter {

    public function WaterFilter() {
        alwaysDrawToBackBuffer = true;
    }

    override protected function createEffect():FilterEffect {
        return new WaterMeshEffect();
    }

    override protected function onTargetAssigned(target:DisplayObject):void {
        super.onTargetAssigned(target);
        addEventListener(Event.ENTER_FRAME, enterFrameHandler);
    }

    private function enterFrameHandler(event:Event):void {
        setRequiresRedraw();
    }
}
}
