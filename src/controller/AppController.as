package controller {
import model.Model;

import view.Screens;

public class AppController {

    private var _model:Model;

    public function AppController(model:Model) {
        _model = model;
    }

    public function showMain():void {
        _model.currentScreen = Screens.MAIN_SCREEN;
    }
}
}
