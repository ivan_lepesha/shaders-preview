package model {
import assets.Images;

import core.filters.ExtFilter;
import core.filters.WaterFilter;
import core.filters.WhiteToColorFilter;

import feathers.data.ArrayCollection;

import starling.events.EventDispatcher;
import starling.textures.Texture;

public class Model extends EventDispatcher {

    private var _currentScreen:String;
    private var _currentFilter:Class;
    private var _currentTexture:Texture;

    public function Model() {
        super();
        _currentTexture = Images.getInstance().getTexture(Images.wood);
        _currentFilter = ExtFilter;
    }

    public function get currentScreen():String {
        return _currentScreen;
    }

    public function set currentScreen(value:String):void {
        if(value != _currentScreen) {
            _currentScreen = value;
            dispatchEventWith("currentScreenChanged");
        }
    }

    public function imagesDataProvider():ArrayCollection {
        var textures:ArrayCollection = new ArrayCollection(
                [
                    { text: "Wood", texture:Images.wood },
                    { text: "Zebra", texture:Images.zebra},
                    { text: "Bamboo", texture:Images.bamboo },
                    { text: "Water", texture:Images.water }
                ]);
        return textures;
    }

    public function filtersDataProvider():ArrayCollection {
        var filters:ArrayCollection = new ArrayCollection(
                [
                    { text: "ExtFilter", filter:ExtFilter },
                    { text: "WaterFilter", filter:WaterFilter },
                    { text: "WhiteToColorFilter", filter:WhiteToColorFilter }
                ]);
        return filters;
    }

    public function get currentFilter():Class {
        return _currentFilter;
    }

    public function set currentFilter(value:Class):void {
        if(value != _currentFilter) {
            _currentFilter = value;
            dispatchEventWith("filterChanged");
        }
    }

    public function get currentTexture():Texture {
        return _currentTexture;
    }

    public function set currentTexture(value:Texture):void {
        if(value != _currentTexture) {
            _currentTexture = value;
            dispatchEventWith("textureChanged");
        }
    }
}
}
