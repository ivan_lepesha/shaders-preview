package core.effects
{
	import flash.display3D.Context3D;
	import flash.display3D.Context3DProgramType;
	import flash.utils.getTimer;

	import starling.rendering.MeshEffect;
	import starling.rendering.Program;
	import starling.rendering.VertexDataFormat;
	import starling.rendering.FilterEffect

	public class ExtMeshEffect extends MeshEffect
	{
		public static const VERTEX_FORMAT:VertexDataFormat = FilterEffect.VERTEX_FORMAT.extend("color:bytes4");

		private var _centerX:Number;
		private var _centerY:Number;

		public function ExtMeshEffect(cX:Number = 0.5, cY:Number = 0.5)
		{
			super();

			_centerX = cX;
			_centerY = cY;
		}

		override protected function beforeDraw(context:Context3D):void
		{
			var t:Number = getTimer() / 1000;

			var fc0:Vector.<Number> = new <Number>[t, 10.0, 0.8, 0.1];
			var fc1:Vector.<Number> = new <Number>[0.5, 1.0, 40.0, 0.0];
			var fc2: Vector.<Number> = new <Number>[_centerX, _centerY, texture.width, texture.height];

			//fc0 [time, 10.0, 0.8, 0.1]; time, wave params
			//fc1 [0.5, 1.0, 40.0, 0.0]; constants
			//fc2 [centerX, centerY, resolutionWidth, resolutionHeight];

			context.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 0, fc0);
			context.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 1, fc1);
			context.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 2, fc2);

			super.beforeDraw(context);
		}

		override protected function createProgram():Program
		{
			var vertexShader:String;
			var fragmentShader:String;

			if (texture)
			{
				vertexShader =
						"m44 op, va0, vc0 \n" + // 4x4 matrix transform to output clip-space
						"mov v0, va1      \n" + // pass texture coordinates to fragment program
						"mul v1, va2, vc4 \n";  // multiply alpha (vc4) with color (va2), pass to fp

				fragmentShader =
						//vec2 texCoord = fragCoord.xy / iResolution.xy; //uv v0.xy
						"mov ft7.xy, v0.xy \n" + // texCoord=ft7.xy

						//float ratio = iResolution.y / iResolution.x;
//						"mov ft0.zw, fc2.zw \n" + // WaveCentre = ft0.zw //todo
						"mov ft0.zw, fc1.xx \n" + // WaveCentre = (0.5, 0.5)
						"div ft0.x, ft0.w, ft0.z \n" + // ratio=ft0.x

						//float offset = (iTime- floor(iTime))/iTime; // source1 - (float)floor(source1)
						"mov ft1.x, fc0.x \n" + // iTime=ft1.x
						"frc ft1.x, ft1.x \n" +
						"div ft1.x, ft1.x, fc0.x \n" + //offset=ft1.x

						//float CurrentTime = iTime * offset;
						"mul ft1.y, ft1.x, fc0.x \n" +// CurrentTime=ft1.y

						//WaveCentre.y = WaveCentre.y * ratio;
//						"mul ft0.w, ft0.w, ft0.x \n" +

						//texCoord.y = texCoord.y * ratio;
						"mul ft7.y, ft7.y, ft0.x\n" +

						//float Dist = distance(texCoord, WaveCentre);
						"sub ft3.x, ft7.x, ft0.z\n" +
						"mul ft3.x, ft3.x, ft3.x\n" +
						"sub ft3.y, ft7.y, ft0.w\n" +
						"mul ft3.y, ft3.y, ft3.y\n" +
						"add ft3.z, ft3.x, ft3.y\n" +
						"sqt ft3.z, ft3.z \n" +  //distance = ft3.z

						"add ft2.x, ft1.y, fc0.w \n" +//CurrentTime + WaveParams.z
						"sub ft2.y, ft1.y, fc0.w \n" +//CurrentTime - WaveParams.z
						"slt ft2.z, ft3.z, ft2.x \n" + //
						"sge ft2.w, ft3.z, ft2.y  \n" +
						"mul ft2.x, ft2.z, ft2.w \n" +
						"ife ft2.x, fc1.y \n" + //IF ((Dist < (CurrentTime + WaveParams.z)) && (Dist >= (CurrentTime - WaveParams.z)))
						"sub ft5.x, ft3.z, ft1.y\n" + // float Diff = (Dist - CurrentTime) Diff=ft5.x
						"mul ft5.y, ft5.x, fc0.y \n" +// Diff * WaveParams.x
						"abs ft5.y, ft5.y \n" +// abs(Diff * WaveParams.x)
						"pow ft5.y, ft5.y, fc0.z \n" + // pow(abs(Diff * WaveParams.x), WaveParams.y)

						"sub ft5.y, fc1.y, ft5.y \n" +// float ScaleDiff = 1.0 - pow(abs(Diff * WaveParams.x), WaveParams.y); ft5.y ScaleDiff
						"mul ft5.z, ft5.x, ft5.y \n" + // float DiffTime = (Diff  * ScaleDiff); ft5.z DiffTime
						"sub ft6.xy, ft7.xy, ft0.zw\n" + // texCoord - WaveCentre
						"mov ft6.zw, fc1.ww \n" + //
						"nrm ft6.xyz, ft6.xyzw\n" + //vec2 DiffTexCoord = normalize(texCoord - WaveCentre);  DiffTexCoord ft6.xy
						"mul ft5.w, ft1.y, ft3.z\n" + //CurrentTime * Dist
						"mul ft5.w, ft5.w, fc1.z \n" +//CurrentTime * Dist * 40.0
						"mul ft7.zw, ft6.xy, ft5.zz \n" +// DiffTexCoord * DiffTime
						"div ft7.zw, ft7.zw, ft5.ww \n" +// ((DiffTexCoord * DiffTime) / (CurrentTime * Dist * 40.0));
						"add ft7.xy, ft7.zw, ft7.xy \n" +// texCoord += ((DiffTexCoord * DiffTime) / (CurrentTime * Dist * 40.0));
						"els \n" + // ELSE
						"mov ft5.y, fc1.y \n" + //ScaleDiff = 1.0
						"mov ft5.w, fc1.y \n" + //(CurrentTime * Dist * 40.0) = 1.0
						"eif \n" + // END IF

						"tex ft4, ft7.xy, fs0 <linear 2d repeat> \n" +//Color = texture(iChannel0, texCoord);

						"ife ft2.x, fc1.y \n" +
						"mul ft8, ft4, ft5.yyyy \n" +// Color * ScaleDiff
						"div ft8, ft8, ft5.wwww \n" +// (Color * ScaleDiff) / (CurrentTime * Dist * 40.0);
						"add ft4, ft4, ft8 \n" +
						"eif \n" + // END IF

						"mov oc, ft4 \n";
			}
			else
			{
				vertexShader =
						"m44 op, va0, vc0 \n" + // 4x4 matrix transform to output clipspace
						"mul v0, va2, vc4 \n";  // multiply alpha (vc4) with color (va2)

				fragmentShader =
						"mov oc, v0       \n";  // output color
			}

			return Program.fromSource(vertexShader, fragmentShader, 2);
		}

		override public function get vertexFormat():VertexDataFormat
		{
			return VERTEX_FORMAT;
		}
	}
}
