package core.effects
{
import flash.display3D.Context3D;
import flash.display3D.Context3DProgramType;
import flash.utils.ByteArray;
import flash.utils.getTimer;

import starling.rendering.Program;
import starling.rendering.VertexDataFormat;
import starling.rendering.FilterEffect

public class CurrentFilterEffect extends FilterEffect
{
    public static const VERTEX_FORMAT:VertexDataFormat = FilterEffect.VERTEX_FORMAT.extend("color:bytes4");

    private var _centerX:Number;
    private var _centerY:Number;
    private var _vShader:ByteArray;
    private var _fShader:ByteArray;

    public function CurrentFilterEffect(vShader:ByteArray, fShader:ByteArray, cX:Number = 0.5, cY:Number = 0.5)
    {
        super();

        _centerX = cX;
        _centerY = cY;

        _vShader = vShader;
        _fShader = fShader;
    }

    override protected function beforeDraw(context:Context3D):void
    {
        var t:Number = getTimer() / 1000;

        var fc0:Vector.<Number> = new <Number>[t, 10.0, 0.8, 0.1];
        var fc1:Vector.<Number> = new <Number>[0.5, 1.0, 40.0, 0.0];
        var fc2: Vector.<Number> = new <Number>[_centerX, _centerY, texture.width, texture.height];

        //fc0 [time, 10.0, 0.8, 0.1]; time, wave params
        //fc1 [0.5, 1.0, 40.0, 0.0]; constants
        //fc2 [centerX, centerY, resolutionWidth, resolutionHeight];

        context.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 0, fc0);
        context.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 1, fc1);
        context.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 2, fc2);

        super.beforeDraw(context);
    }

    override protected function createProgram():Program
    {
        var vertexShader:String;
        var fragmentShader:String;
        var result:Program;

        if (texture)
        {
            vertexShader =
                    "m44 op, va0, vc0 \n" +
                    "mov v0, va1      \n" +
                    "mul v1, va2, vc4 \n";

            fragmentShader =
                    "mov ft0.xy, v0.xy \n" +
                    "mul ft0.xy, ft0.xy, fc0.yy \n" +
                    "add ft0.xy, ft0.xy, fc0.xx \n" +

                    "add ft2.x, ft0.x, ft0.y  \n" +
                    "cos ft2.x, ft2.x  \n" +

                    "sub ft2.y, ft0.x, ft0.y  \n" + //x - y
                    "sin ft2.y, ft2.y \n" + //sin(x - y)

                    "cos ft2.z, ft0.y  \n" + //cos(y)
                    "sin ft2.w, ft0.y  \n" + //sin(y)

                    "mul ft3.x, ft2.x, fc0.z  \n" + // cos(x + y) * 0.01
                    "mul ft3.y, ft2.y, fc0.z  \n" + // sin(x - y) * 0.01

                    "mul ft5.y, ft3.x, ft2.z  \n" + //cos(x + y) * 0.01 * cos(y)
                    "mul ft5.x, ft3.y, ft2.w  \n" + //sin(x - y) * 0.01 * sin(y)

                    "mov ft6.xy, v0.xy \n" + //mov uv
                    "add ft6.x, ft6.x, ft5.y \n" + //uv.x + (sin(x - y) * 0.01 * sin(y))
                    "add ft6.y, ft6.y, ft5.x \n" + //uv.y + (cos(x + y) * 0.01 * cos(y))

                    "tex ft4, ft6.xy, fs0 <linear 2d repeat>\n" +
                    "mov oc ft4\n";
        }
        else
        {
            vertexShader =
                    "m44 op, va0, vc0 \n" + // 4x4 matrix transform to output clipspace
                    "mul v0, va2, vc4 \n";  // multiply alpha (vc4) with color (va2)

            fragmentShader =
                    "mov oc, v0       \n";  // output color
        }

        result = Program.fromSource(vertexShader, fragmentShader, 2);

        return result;
    }

    override public function get vertexFormat():VertexDataFormat
    {
        return VERTEX_FORMAT;
    }
/*
mov ft0.xy, v0.xy
mul ft0.xy, ft0.xy, fc0.yy
add ft0.xy, ft0.xy, fc0.xx
add ft2.x, ft0.x, ft0.y
cos ft2.x, ft2.x
sub ft2.y, ft0.x, ft0.y
sin ft2.y, ft2.y
cos ft2.z, ft0.y
sin ft2.w, ft0.y
mul ft3.x, ft2.x, fc0.z
mul ft3.y, ft2.y, fc0.z
mul ft5.y, ft3.x, ft2.z
mul ft5.x, ft3.y, ft2.w
mov ft6.xy, v0.xy
add ft6.x, ft6.x, ft5.y
add ft6.y, ft6.y, ft5.x
tex ft4, ft6.xy, fs0 <linear 2d repeat>
mov oc ft4\n
*/
}
}
