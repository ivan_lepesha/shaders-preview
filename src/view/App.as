package view {
import controller.AppController;

import feathers.themes.MetalWorksDesktopTheme;

import model.Model;

import starling.display.Sprite;

public class App extends Sprite {

    private var _navigator:AppScreenNavigator;
    private var _model:Model;
    private var _controller:AppController;

    public function App() {
        _model = new Model();
        _controller = new AppController(_model) as AppController;

        new MetalWorksDesktopTheme();
    }

    public function run():void {
        createNavigator();
    }

    private function createNavigator():void {
        _navigator = new AppScreenNavigator();
        _navigator.init(_model, _controller);
        addChild(_navigator);

       _controller.showMain();
    }
}
}
